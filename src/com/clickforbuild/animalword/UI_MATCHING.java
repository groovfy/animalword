package com.clickforbuild.animalword;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.content.Intent;

public class UI_MATCHING extends Activity {
	  float startx,starty;
	  int countscore=0;
	  int interval=1000;
	  int count=0;
		static	long timeused=0;
		long counttime=(1000*60)*1; // 1 Minute
		 Timer timerNoti=null;
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.matching);
	    findViewById(R.id.lbl1).setOnTouchListener(new MyTouchListener());
	    findViewById(R.id.lbl2).setOnTouchListener(new MyTouchListener());
	    findViewById(R.id.lbl3).setOnTouchListener(new MyTouchListener());
	    findViewById(R.id.lbl4).setOnTouchListener(new MyTouchListener());
	    findViewById(R.id.lbl5).setOnTouchListener(new MyTouchListener());
	    
	    findViewById(R.id.right1).setOnDragListener(new MyDragListener());
	    findViewById(R.id.right2).setOnDragListener(new MyDragListener());
	    findViewById(R.id.right3).setOnDragListener(new MyDragListener());
	    findViewById(R.id.right4).setOnDragListener(new MyDragListener());
	    findViewById(R.id.right5).setOnDragListener(new MyDragListener());
	    timerNoti=new Timer();
	    timerNoti.schedule(tt, 0, interval); 
	  }
	  private TimerTask tt=new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				counttime-=interval;
				timeused+=interval;
				 myHandle.sendMessage(myHandle.obtainMessage());
			}
		};
	    Handler myHandle = new Handler(){
	    	
			public void handleMessage(android.os.Message msg) {
				//lbltime.setText(convertTimeToString(counttime));
				if(counttime <=0)
				{
					//isgameover=true;
					timerNoti.cancel();
					//showGameOverDialog();
				}		
			}
	    };

	    private final class MyTouchListener implements OnTouchListener {
	    public boolean onTouch(View view, MotionEvent motionEvent) {
	      if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
	    	  startx=view.getX();
	    	  starty=view.getY();
	        ClipData data = ClipData.newPlainText("", "");
	        DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
	        view.startDrag(data, shadowBuilder, view, 0);
	        view.setVisibility(View.INVISIBLE);
	        
	        return true;
	      } else {
	        return false;
	      }
	    }
	  }
	  public void showInfoDialog(String str, Context context) {
			final Dialog dialog = new Dialog(context);
			LinearLayout layout = new LinearLayout(context);
			layout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));
			layout.setOrientation(LinearLayout.VERTICAL);
			TextView lblinfo = new TextView(context);
			lblinfo.setMinLines(3);
			lblinfo.setText(str);
			lblinfo.setTextSize(28);
			lblinfo.setTextColor(Color.RED);
			Button btnOK = new Button(context);
			btnOK.setText("OK");
			btnOK.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					
					dialog.dismiss();
				}
			});
			layout.addView(lblinfo);
			layout.addView(btnOK);
			dialog.setContentView(layout);
			dialog.setTitle("Result Score");
			dialog.show();
			
			Intent i = new Intent(UI_MATCHING.this, MainActivity.class);
			startActivity(i);
		}
		
	  class MyDragListener implements OnDragListener {
	    Drawable enterShape = getResources().getDrawable(R.drawable.shape_droptarget);
	    Drawable normalShape = getResources().getDrawable(R.drawable.shape);

	    @Override
	    public boolean onDrag(View v, DragEvent event) {
	      int action = event.getAction();
	      switch (event.getAction()) {
	      case DragEvent.ACTION_DRAG_STARTED:
	        // Do nothing
	        break;
	      case DragEvent.ACTION_DRAG_ENTERED:
	        v.setBackgroundDrawable(enterShape);
	        break;
	      case DragEvent.ACTION_DRAG_EXITED:
	        v.setBackgroundDrawable(normalShape);
	        break;
	      case DragEvent.ACTION_DROP:
	    	  
	        // Dropped, reassign View to ViewGroup
	        View view = (View) event.getLocalState();
	        ViewGroup owner = (ViewGroup) view.getParent();
	        owner.removeView(view);
	        String tag1=view.getTag().toString();
	        LinearLayout container = (LinearLayout) v;
	        container.addView(view);
	        view.setVisibility(View.VISIBLE);
	        String tag2=container.getTag().toString();
	        System.out.println("tag1="+tag1+",tag2="+tag2);
	        if(tag1.equalsIgnoreCase(tag2))
	        {
	        	countscore++;
	        	System.out.println(countscore);
	        }
	        count++;
	        System.out.println("count="+count);
	    	if(count ==5)
			{
				System.out.println("showscore");
				showInfoDialog("Your score is "+countscore, UI_MATCHING.this);
		    	
			}
	        break;
	      case DragEvent.ACTION_DRAG_ENDED:
	        v.setBackgroundDrawable(normalShape);
	        
	      default:
	        break;
	      }
	      return true;
	    }
	  }
}
